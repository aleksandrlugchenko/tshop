const path = require('path');
const ESLintPlugin = require('eslint-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const WorkboxWebpackPlugin = require('workbox-webpack-plugin');

require('dotenv').config();

module.exports = {
  entry: './src/index.tsx',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          presets: [
            '@babel/preset-env',
            [
              '@babel/preset-react',
              {
                runtime: 'automatic',
              },
            ],
            '@babel/preset-typescript',
          ],
        },
      },
      {
        test: /\.module\.s[ac]ss$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: {
                localIdentName: '[local]_[hash:base64:5]',
              },
            },
          },
          'sass-loader',
        ],
      },
      {
        test: /\.s[ac]ss$/,
        exclude: /\.module\.s[ac]ss$/,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      },
      {
        test: /\.(png|jpe?g|gif|webp)$/i,
        use: [
          {
            loader: 'file-loader',
            options: {
              outputPath: 'images',
            },
          },
        ],
      },
      {
        test: /\.svg$/,
        use: ['@svgr/webpack', 'file-loader'],
      },
    ],
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
    alias: {
      pages: path.resolve(__dirname, '../src/pages'),
      utils: path.resolve(__dirname, '../src/utils'),
      components: path.resolve(__dirname, '../src/components'),
      containers: path.resolve(__dirname, '../src/containers'),
      constants: path.resolve(__dirname, '../src/constants'),
      types: path.resolve(__dirname, '../src/types'),
      locales: path.resolve(__dirname, '../src/locales'),
      styles: path.resolve(__dirname, '../src/styles'),
      ui: path.resolve(__dirname, '../src/ui'),
      routes: path.resolve(__dirname, '../src/routes'),
      store: path.resolve(__dirname, '../src/store'),
      slices: path.resolve(__dirname, '../src/store/slices'),
      selectors: path.resolve(__dirname, '../src/store/selectors'),
      services: path.resolve(__dirname, '../src/store/services'),
    },
  },
  output: {
    path: path.resolve(__dirname, '../dist'),
    publicPath: `/`,
    filename: '[name].[contenthash].js',
  },
  plugins: [
    new ESLintPlugin({}),
    new HtmlWebpackPlugin({
      template: './public/index.html',
    }),
    new HtmlWebpackPlugin({
      template: './public/404.html',
      filename: '404.html',
    }),
  ],
};
