const { merge } = require('webpack-merge');
// @ts-ignore
const commonConfig = require('./webpack.config.js');
require('dotenv').config();

module.exports = merge(commonConfig, {
  mode: 'development',
  devtool: 'inline-source-map',
  devServer: {
    port: process.env.PORT || 3000,
    hot: true,
    open: true,
    historyApiFallback: true,
  },
});
