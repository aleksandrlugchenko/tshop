export const ALL_PRODUCTS_ROUTE = '/all-products';
export const PRODUCTS_BY_CATEGORY_ROUTE = '/category/:category';

export const PRODUCT_PAGE_ROUTE = `${PRODUCTS_BY_CATEGORY_ROUTE}/product/:id`;

export const getProductsByCategoryRoute = (category: string) => `/category/${category}`;

export const getProductPageRoute = (id: number, category: string) =>
  `${getProductsByCategoryRoute(category)}/product/${id}`;
