import { Navigate } from 'react-router-dom';

import AllProducts from 'pages/all-products';
import { Login, Registration } from 'pages/auth';
import ProductPage from 'pages/product-page';
import ProductsByCategory from 'pages/products-by-category';

import { RouteType } from 'types/routes.types';

import { ALL_PRODUCTS_ROUTE, PRODUCTS_BY_CATEGORY_ROUTE, PRODUCT_PAGE_ROUTE } from './app/list';
import { LOGIN_ROUTE, REGISTER_ROUTE } from './auth/list';

export const HOME_NON_AUTH_ROUTE = LOGIN_ROUTE;
export const UNKNOWN_ROUTE = '*';

export const AUTH_ROUTES: RouteType[] = [
  {
    path: ALL_PRODUCTS_ROUTE,
    element: <AllProducts />,
  },
  { path: PRODUCTS_BY_CATEGORY_ROUTE, element: <ProductsByCategory /> },
  { path: PRODUCT_PAGE_ROUTE, element: <ProductPage /> },
  { path: UNKNOWN_ROUTE, element: <Navigate to={ALL_PRODUCTS_ROUTE} /> },
];

export const NON_AUTH_ROUTES: RouteType[] = [
  {
    path: LOGIN_ROUTE,
    element: <Login />,
  },
  {
    path: REGISTER_ROUTE,
    element: <Registration />,
  },
  { path: UNKNOWN_ROUTE, element: <Navigate replace to={HOME_NON_AUTH_ROUTE} /> },
];
