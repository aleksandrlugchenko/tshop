export const LOGIN_ROUTE = '/login';
export const REGISTER_ROUTE = '/register';

export const FORGOT_PASSWORD_ROUTE = '/reset-password';
