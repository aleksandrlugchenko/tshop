import { FC, useEffect } from 'react';

import { useParams } from 'react-router-dom';
import { ALL_PRODUCTS_ROUTE, getProductsByCategoryRoute } from 'routes/app/list';
import { Breadcrumbs, Icon } from 'ui';
import { cutString } from 'utils/cutString';
import { firstLetterToUpperCase } from 'utils/firstLetterToUpperCase';

import { useLazyGetSingleProductQuery } from 'store/services/products/productsApiService';

import s from './ProductPage.module.scss';

const ProductPage: FC = (props) => {
  const { category, id } = useParams();
  const [getSingleProduct, { data: product }] = useLazyGetSingleProductQuery();
  const BREADCRUMBS_ROUTES = [
    { name: 'All products', path: ALL_PRODUCTS_ROUTE },
    { name: firstLetterToUpperCase(category), path: getProductsByCategoryRoute(category) },
    { name: cutString(firstLetterToUpperCase(product?.title || ''), 10) },
  ];

  useEffect(() => {
    getSingleProduct(Number(id));
  }, []);

  return (
    <div className={s.wrapper}>
      <div className={s.header}>
        <Breadcrumbs routes={BREADCRUMBS_ROUTES} />
      </div>

      <div className={s.content}>
        <div>
          <div className={s.name}>{firstLetterToUpperCase(product?.title || '')}</div>
          <div className={s.rating}>
            <div>{product?.rating.count} sold</div>
            <div className={s.rate}>
              <Icon name="star" width={16} height={16} />
              {product?.rating.rate}
            </div>
          </div>
        </div>
        <div className={s.desc}>
          <img src={product?.image} alt={product?.title} className={s.img} />
          <div className={s.info}>
            <div className={s.item}>
              <span className={s.label}>Price</span>
              <span className={s.price}>€ {product?.price}</span>
            </div>
            <div className={s.item}>
              <span className={s.label}>Description</span>
              <span className={s.text}>{product?.description}</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductPage;
