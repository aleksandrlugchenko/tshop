import { FC, useEffect, useState } from 'react';

import { useParams } from 'react-router-dom';

import { useLazyGetProductsByCategoryQuery } from 'store/services/products/productsApiService';

import ProductItem from 'components/product-item';

import s from './ProductsByCategory.module.scss';
import ProductsByCategorySorting from './product-by-category-sorting';
import ProductsByCategoryFilters from './products-by-category-filters';
import ProductsByCategoryHeader from './products-by-category-header';

const ProductsByCategory: FC = () => {
  const { category } = useParams();
  const [getProductsByCategory, { data: products }] = useLazyGetProductsByCategoryQuery();
  const [isOpenedSorting, setIsOpenedSorting] = useState(null);
  const [isOpenedFilters, setIsOpenedFilters] = useState(null);

  useEffect(() => {
    getProductsByCategory({ category });
  }, [category]);

  return (
    <div className={s.wrapper}>
      <ProductsByCategoryHeader
        category={category}
        setIsOpenedSorting={setIsOpenedSorting}
        setIsOpenedFilters={setIsOpenedFilters}
      />

      <div className={s.products}>
        {products?.map((product) => <ProductItem key={product.id} product={product} fullWidth />)}
      </div>

      <ProductsByCategorySorting opened={isOpenedSorting} setIsOpenedSorting={setIsOpenedSorting} />
      <ProductsByCategoryFilters opened={isOpenedFilters} setIsOpenedFilters={setIsOpenedFilters} />
    </div>
  );
};

export default ProductsByCategory;
