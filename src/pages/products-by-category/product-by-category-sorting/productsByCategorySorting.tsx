import { Dispatch, FC, SetStateAction } from 'react';

import { Radio as RadioAntd } from 'antd';
import cn from 'classnames';
import { Radio } from 'ui';

import s from './ProductsByCategorySorting.module.scss';

const RadioGroup = RadioAntd.Group;
interface ProductsByCategorySortingProps {
  opened?: boolean;
  setIsOpenedSorting: Dispatch<SetStateAction<boolean>>;
}

const ProductsByCategorySorting: FC<ProductsByCategorySortingProps> = (props) => {
  const { opened = false, setIsOpenedSorting } = props;

  const handleClose = () => {
    setIsOpenedSorting(false);
  };

  return (
    <div className={cn(s.wrapper, { [s.opened]: opened, [s.closed]: opened === false })}>
      <h3>Sorting</h3>
      <button className={s.action} onClick={handleClose}>
        Done
      </button>

      <div className={s.content}>
        <RadioGroup>
          <div className={s.sorting}>
            <Radio value={1}>
              <span className={s.label}>Price: low to high</span>
            </Radio>
            <Radio value={2}>
              <span className={s.label}>Price: high to low</span>
            </Radio>
            <Radio value={3}>
              <span className={s.label}>Most recent</span>
            </Radio>
            <Radio value={4}>
              <span className={s.label}>Name: A to Z</span>
            </Radio>
            <Radio value={5}>
              <span className={s.label}>Name: Z to A</span>
            </Radio>
            <Radio value={6}>
              <span className={s.label}>Rating: high to low</span>
            </Radio>
          </div>
        </RadioGroup>
      </div>
    </div>
  );
};

export default ProductsByCategorySorting;
