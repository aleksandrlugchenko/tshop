import { Dispatch, FC, SetStateAction } from 'react';

import { Radio as RadioAntd } from 'antd';
import cn from 'classnames';
import { Checkbox, Input, Radio } from 'ui';
import { firstLetterToUpperCase } from 'utils/firstLetterToUpperCase';

import { useGetCategoriesQuery } from 'store/services/products/productsApiService';

import s from './ProductsByCategoryFilters.module.scss';

const RadioGroup = RadioAntd.Group;
interface ProductsByCategorySortingProps {
  opened?: boolean;
  setIsOpenedFilters: Dispatch<SetStateAction<boolean>>;
}

const ProductsByCategoryFilters: FC<ProductsByCategorySortingProps> = (props) => {
  const { opened = false, setIsOpenedFilters } = props;
  const { data: categories } = useGetCategoriesQuery();

  const handleClose = () => {
    setIsOpenedFilters(false);
    document.body.style.overflow = 'unset';
  };

  return (
    <div className={cn(s.wrapper, { [s.opened]: opened, [s.closed]: opened === false })}>
      <h3>Filters</h3>
      <button className={s.action} onClick={handleClose}>
        Done
      </button>

      <div className={s.content}>
        <div className={s.label}>Price</div>
        <div className={s.price}>
          <Input name="priceFrom" placeholder="€ 100" />
          <span className={s.divider}>-</span>
          <Input name="priceTo" placeholder="€ 1000" />
        </div>
        <RadioGroup>
          <div className={s.label}>Condition</div>
          <div className={s.sorting}>
            <Radio value={1}>
              <span className={s.label}>New</span>
            </Radio>
            <Radio value={2}>
              <span className={s.label}>Resale</span>
            </Radio>
          </div>
        </RadioGroup>

        <div className={s.categories}>
          <div className={s.label}>Categories</div>
          {categories?.map((category) => (
            <Checkbox key={category} value={category}>
              <span className={s.label}>{firstLetterToUpperCase(category)}</span>
            </Checkbox>
          ))}
        </div>
      </div>
    </div>
  );
};

export default ProductsByCategoryFilters;
