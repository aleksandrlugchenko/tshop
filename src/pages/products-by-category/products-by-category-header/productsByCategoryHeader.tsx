import { Dispatch, FC, SetStateAction } from 'react';

import { ALL_PRODUCTS_ROUTE } from 'routes/app/list';
import { Breadcrumbs, Icon } from 'ui';
import { firstLetterToUpperCase } from 'utils/firstLetterToUpperCase';

import s from './ProductsByCategoryHeader.module.scss';

interface ProductsByCategoryFiltersProps {
  category: string;
  setIsOpenedSorting: Dispatch<SetStateAction<boolean>>;
  setIsOpenedFilters: Dispatch<SetStateAction<boolean>>;
}

const ProductsByCategoryHeader: FC<ProductsByCategoryFiltersProps> = (props) => {
  const { category, setIsOpenedSorting, setIsOpenedFilters } = props;
  const BREADCRUMBS_ROUTES = [
    { name: 'All products', path: ALL_PRODUCTS_ROUTE },
    { name: firstLetterToUpperCase(category) },
  ];

  const handleOpenSorting = () => {
    setIsOpenedSorting(true);
  };

  const handleOpenFilters = () => {
    setIsOpenedFilters(true);
    document.body.style.overflow = 'hidden';
  };

  return (
    <div className={s.wrapper}>
      <Breadcrumbs routes={BREADCRUMBS_ROUTES} />

      <div className={s.actions}>
        <button className={s.action} onClick={handleOpenSorting}>
          <Icon name="sorting" size={20} />
        </button>
        <button className={s.action} onClick={handleOpenFilters}>
          <Icon name="filters" size={20} />
        </button>
      </div>
    </div>
  );
};

export default ProductsByCategoryHeader;
