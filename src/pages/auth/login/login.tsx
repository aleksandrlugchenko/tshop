import { FC } from 'react';

import { useLoginMutation } from 'store/services/user-management/userManagementApiService';

import { LoginFormDataType } from 'types/auth.types';

import LoginForm from './login-form';

const Login: FC = () => {
  const [login, { isLoading }] = useLoginMutation();
  const handleSubmit = async (data: LoginFormDataType) => {
    const res = await login({
      password: data.password,
      username: data.email,
    });
  };

  return <LoginForm onSubmit={handleSubmit} isLoading={isLoading} />;
};

export default Login;
