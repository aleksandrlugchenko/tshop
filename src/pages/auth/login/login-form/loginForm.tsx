import { FC } from 'react';

import cn from 'classnames';
import { Link } from 'react-router-dom';
import { REGISTER_ROUTE } from 'routes/auth/list';
import { Button, Form, Input } from 'ui';
import { formRules } from 'utils/formRules';

import { LoginFormDataType } from 'types/auth.types';

import s from '../../Auth.module.scss';

interface LoginFormProps {
  onSubmit: (data: LoginFormDataType) => void;
  isLoading: boolean;
}

const LoginForm: FC<LoginFormProps> = ({ onSubmit, isLoading }) => (
  <>
    <Form className={cn(s.form, s.login)} onFinish={onSubmit}>
      <Form.Item fullWidth name="email" rules={[formRules.required]} initialValue="mor_2314">
        <Input name="email" label="Email" placeholder="Enter your username" />
      </Form.Item>

      <Form.Item fullWidth name="password" rules={[formRules.required]} initialValue="83r5^_">
        <Input.Password name="password" label="Password" placeholder="Enter your password" />
      </Form.Item>

      <Button
        type="primary"
        fullWidth
        className={s.submitBtn}
        htmlType="submit"
        disabled={isLoading}
        isLoading={isLoading}>
        Login
      </Button>

      <div className={s.registerQuestion}>
        Don't have an account?{' '}
        <Link to={REGISTER_ROUTE} className={s.link}>
          Register here
        </Link>
      </div>
    </Form>
  </>
);

export default LoginForm;
