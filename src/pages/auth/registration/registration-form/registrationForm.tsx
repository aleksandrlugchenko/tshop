import { FC } from 'react';

import { Link } from 'react-router-dom';
import { LOGIN_ROUTE } from 'routes/auth/list';
import { Button, Form, Input } from 'ui';
import { formRules } from 'utils/formRules';

import { RegistrationFormDataType } from 'types/auth.types';

import s from '../../Auth.module.scss';

interface RegistrationFormProps {
  onSubmit: (data: RegistrationFormDataType) => void;
}
const RegistrationForm: FC<RegistrationFormProps> = ({ onSubmit }) => (
  <>
    <Form className={s.form} onFinish={onSubmit}>
      <Form.Item fullWidth name="firstName" rules={[formRules.required]}>
        <Input name="firstName" label="First name" placeholder="Enter your first name" />
      </Form.Item>

      <Form.Item fullWidth name="lastName" rules={[formRules.required]}>
        <Input name="lastName" label="Last name" placeholder="Enter your last name" />
      </Form.Item>

      <Form.Item fullWidth name="email" rules={[formRules.required, formRules.email]}>
        <Input name="email" label="Email" placeholder="Enter your email" />
      </Form.Item>

      <Form.Item fullWidth name="password" rules={[formRules.password]}>
        <Input.Password name="password" label="Password" placeholder="Enter your password" />
      </Form.Item>

      <Button type="primary" fullWidth className={s.submitBtn} htmlType="submit">
        Register
      </Button>

      <div className={s.registerQuestion}>
        Already have an account?
        <Link to={LOGIN_ROUTE} className={s.link}>
          Log in here
        </Link>
      </div>
    </Form>
  </>
);

export default RegistrationForm;
