import { FC } from 'react';

import { useRegisterMutation } from 'store/services/user-management/userManagementApiService';

import { RegistrationFormDataType } from 'types/auth.types';

import RegistrationForm from './registration-form';

interface RegistrationProps {
  // Define your component props here
}

const Registration: FC<RegistrationProps> = (props) => {
  const [createUser] = useRegisterMutation();
  const handleSubmit = (data: RegistrationFormDataType) => {
    createUser({
      ...data,
      username: data.email,
    });
  };

  return <RegistrationForm onSubmit={handleSubmit} />;
};

export default Registration;
