import { FC, useCallback, useEffect } from 'react';

import { Link, useNavigate } from 'react-router-dom';
import { getProductPageRoute, getProductsByCategoryRoute } from 'routes/app/list';
import { Icon } from 'ui';
import { firstLetterToUpperCase } from 'utils/firstLetterToUpperCase';

import { useGetProductsByCategoryQuery } from 'store/services/products/productsApiService';

import ProductItem from 'components/product-item';

import s from './AllProductsCategoryPreview.module.scss';

interface HomeCategoryPreviewProps {
  title: string;
}

const AllProductsCategoryPreview: FC<HomeCategoryPreviewProps> = (props) => {
  const { title } = props;
  const { data: products } = useGetProductsByCategoryQuery({ category: title, limit: 5 });

  return (
    <div className={s.wrapper}>
      <div className={s.header}>
        <h3>{firstLetterToUpperCase(title)}</h3>
        <Link to={getProductsByCategoryRoute(title)} className={s.link}>
          View all
          <Icon name="simpleArrowRight" width={10} />
        </Link>
      </div>

      <div className={s.content}>
        {products?.map((product) => <ProductItem product={product} key={product.id} />)}
      </div>
    </div>
  );
};

export default AllProductsCategoryPreview;
