import { FC, useEffect } from 'react';

import { useLazyGetCategoriesQuery } from 'store/services/products/productsApiService';

import s from './AllProducts.module.scss';
import AllProductsCategoryPreview from './all-products-category-preview';
import AllProductsFilters from './all-products-filters';

const AllProducts: FC = () => {
  const [getCategories, { data: categories }] = useLazyGetCategoriesQuery();

  useEffect(() => {
    getCategories();
  }, []);

  return (
    <div className={s.wrapper}>
      <AllProductsFilters />

      <div className={s.content}>
        {categories?.map((category) => (
          <AllProductsCategoryPreview key={category} title={category} />
        ))}
      </div>
    </div>
  );
};

export default AllProducts;
