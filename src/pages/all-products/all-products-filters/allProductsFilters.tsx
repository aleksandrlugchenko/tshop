import { FC } from 'react';

import { Breadcrumbs } from 'ui';

import s from './AllProductsFilters.module.scss';

interface HomeFiltersProps {
  // Define your component props here
}

const BREADCRUMBS_ROUTES = [{ name: 'All products' }];
const AllProductsFilters: FC<HomeFiltersProps> = (props) => {
  return (
    <div className={s.wrapper}>
      <Breadcrumbs routes={BREADCRUMBS_ROUTES} />
    </div>
  );
};

export default AllProductsFilters;
