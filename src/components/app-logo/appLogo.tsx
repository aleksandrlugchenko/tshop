import { FC } from 'react';

import { Icon } from 'ui';

import s from './AppLogo.module.scss';

const AppLogo: FC = () => (
  <div className={s.wrapper}>
    <Icon name="logo" width={170} height={45} />
  </div>
);

export default AppLogo;
