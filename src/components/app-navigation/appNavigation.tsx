import { FC } from 'react';

import cn from 'classnames';
import { Link, NavLink } from 'react-router-dom';

import { useAppDispatch } from 'store';
import { useGetCategoriesQuery } from 'store/services/products/productsApiService';
import { setIsSiderOpened } from 'store/slices/appSlice';

import s from './AppNavigation.module.scss';
import { getAppNavigationConfig } from './utils';

const AppNavigation: FC = () => {
  const { data: categories } = useGetCategoriesQuery();
  const dispatch = useAppDispatch();

  const handleClick = () => {
    dispatch(setIsSiderOpened(false));
  };

  return (
    <div className={s.wrapper}>
      {getAppNavigationConfig(categories || []).map((item) => (
        <NavLink
          to={item.link}
          className={({ isActive }) => cn(s.link, { [s.active]: isActive })}
          key={item.title}
          onClick={handleClick}>
          {item.title}
        </NavLink>
      ))}
    </div>
  );
};

export default AppNavigation;
