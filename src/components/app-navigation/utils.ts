import { ALL_PRODUCTS_ROUTE, getProductsByCategoryRoute } from 'routes/app/list';
import { firstLetterToUpperCase } from 'utils/firstLetterToUpperCase';

export const getAppNavigationConfig = (categories: string[]): { title: string; link: string }[] => {
  const categoriesLink = categories.map((category) => ({
    title: firstLetterToUpperCase(category),
    link: getProductsByCategoryRoute(category),
  }));

  return [
    {
      title: 'All products',
      link: ALL_PRODUCTS_ROUTE,
    },
    ...categoriesLink,
  ];
};
