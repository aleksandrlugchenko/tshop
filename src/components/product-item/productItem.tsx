import { FC, useCallback } from 'react';

import cn from 'classnames';
import { useNavigate } from 'react-router-dom';
import { Icon } from 'ui';
import { cutString } from 'utils/cutString';

import { ProductType } from 'types/Product.types';

import { getProductPageRoute } from '../../routes/app/list';
import s from './ProductItem.module.scss';

interface ProductItemProps {
  product: ProductType;
  fullWidth?: boolean;
}

const ProductItem: FC<ProductItemProps> = (props) => {
  const { product, fullWidth = false } = props;
  const navigate = useNavigate();

  const goToProduct = useCallback(() => {
    navigate(getProductPageRoute(product.id, product.category));
  }, [navigate, product.id, product.category]);

  return (
    <div className={cn(s.item, fullWidth && s.fullWidth)} onClick={goToProduct}>
      <img src={product.image} alt={product.title} className={s.img} />

      <div className={s.info}>
        <div className={s.name}>{cutString(product.title)}</div>
        <div className={s.price}>€ {product.price}</div>
        <div className={s.rating}>
          <div>{product.rating.count} sold</div>
          <div className={s.rate}>
            <Icon name="star" width={16} height={16} />
            {product.rating.rate}
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductItem;
