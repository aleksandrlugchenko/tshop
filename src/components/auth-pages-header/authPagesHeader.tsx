import { FC } from 'react';

import AppLogo from '../app-logo';
import s from './AuthPagesHeader.module.scss';

interface AuthPagesHeaderProps {}

const AuthPagesHeader: FC<AuthPagesHeaderProps> = (props) => {
  return (
    <div className={s.wrapper}>
      <AppLogo />
    </div>
  );
};

export default AuthPagesHeader;
