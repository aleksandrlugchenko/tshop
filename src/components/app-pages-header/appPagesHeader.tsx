import { FC } from 'react';

import AppLogo from '../app-logo';
import s from './AppPagesHeader.module.scss';
import AppPagesHeaderBurgerIcon from './app-pages-header-burger-icon';
import AppPagesHeaderSearchInput from './app-pages-header-serach-input';
import AppPagesHeaderThemeSwitcher from './app-pages-header-theme-switcher';

interface AppPagesHeaderProps {
  changeTheme: () => void;
}
const AppPagesHeader: FC<AppPagesHeaderProps> = (props) => {
  const { changeTheme } = props;

  return (
    <div className={s.wrapper}>
      <AppPagesHeaderBurgerIcon />
      <div className={s.logo}>
        <AppLogo />
      </div>

      <AppPagesHeaderSearchInput />

      <AppPagesHeaderThemeSwitcher onClick={changeTheme} />
    </div>
  );
};

export default AppPagesHeader;
