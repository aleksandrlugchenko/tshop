import { FC } from 'react';

import { Input } from 'ui';

import s from './AppPagesHeaderSearchInput.module.scss';

interface AppPagesHeaderSearchInputProps {}

const AppPagesHeaderSearchInput: FC<AppPagesHeaderSearchInputProps> = (props) => {
  return (
    <div className={s.wrapper}>
      <Input name="search" fullWidth icon="search" placeholder="Search product" />
    </div>
  );
};

export default AppPagesHeaderSearchInput;
