import { FC } from 'react';

import { Icon } from 'ui';

import s from './AppPagesHeaderThemeSwitcher.module.scss';

interface AppPagesHeaderThemeSwitcherProps {
  onClick: () => void;
}

const AppPagesHeaderThemeSwitcher: FC<AppPagesHeaderThemeSwitcherProps> = (props) => {
  const { onClick } = props;
  return (
    <div onClick={onClick} className={s.wrapper}>
      <Icon name="changeTheme" />
    </div>
  );
};

export default AppPagesHeaderThemeSwitcher;
