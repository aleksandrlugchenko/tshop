import { FC, useState } from 'react';

import cn from 'classnames';

import { useAppDispatch, useAppSelector } from 'store';
import { isSiderOpenedSelector } from 'store/selectors/appSlice.selectors';
import { setIsSiderOpened } from 'store/slices/appSlice';

import s from './AppPagesHeaderBurgerIcon.module.scss';

const AppPagesHeaderBurgerIcon: FC = (props) => {
  const dispatch = useAppDispatch();
  const isOpenedMenu = useAppSelector(isSiderOpenedSelector);

  const handleClick = () => {
    dispatch(setIsSiderOpened(!isOpenedMenu));
  };

  return (
    <button className={cn(s.burger, { [s.open]: isOpenedMenu })} onClick={handleClick}>
      <span></span>
      <span className=""></span>
      <span></span>
    </button>
  );
};

export default AppPagesHeaderBurgerIcon;
