import { SwitchProps } from 'antd';

import Switch from './switch';

export type { SwitchProps };
export default Switch;
