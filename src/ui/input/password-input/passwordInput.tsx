import { FC } from 'react';

import { Input as InputAntd, InputProps as InputPropsAntd } from 'antd';
import cn from 'classnames';

import 'antd/lib/input/style/index';

import s from '../Input.module.scss';

export interface PasswordInputProps extends Omit<InputPropsAntd, 'size'> {
  name: string;
  label?: string;
  size?: 'small' | 'middle' | 'large';
  className?: string;
  fullWidth?: boolean;
}

const PasswordInput: FC<PasswordInputProps> = (props) => {
  const {
    value,
    size = 'middle',
    disabled = false,
    name,
    placeholder,
    fullWidth = false,
    onChange,
    className = '',
    label,
    ...otherProps
  } = props;

  return (
    <div className={cn(s.container, { [s.disabled]: disabled, [s.fullWidth]: fullWidth })}>
      {label && <div className={s.label}>{label}</div>}
      <InputAntd.Password
        name={name}
        className={cn(s.input, s[size], {
          [className]: className,
        })}
        placeholder={placeholder}
        value={value}
        disabled={disabled}
        onChange={onChange}
        id={name}
        {...otherProps}
      />
    </div>
  );
};

export default PasswordInput;
