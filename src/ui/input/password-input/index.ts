import PasswordInput, { PasswordInputProps } from './passwordInput';

export type { PasswordInputProps };
export default PasswordInput;
