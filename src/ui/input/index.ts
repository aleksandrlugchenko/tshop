import { InputRef } from 'antd';

import InternalInput, { InputProps } from './input';
import PasswordInput, { PasswordInputProps } from './password-input';

type CompoundedComponents = typeof InternalInput & {
  Password: typeof PasswordInput;
};

const Input = InternalInput as CompoundedComponents;
Input.Password = PasswordInput;

export type { InputProps, InputRef, PasswordInputProps };
export default Input;
