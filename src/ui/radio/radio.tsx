import { FC } from 'react';

import { Radio as RadioAntd } from 'antd';
import { RadioProps as RadioPropsAntd } from 'antd/lib/radio';

import s from './Radio.module.scss';

interface RadioProps extends RadioPropsAntd {}

const Radio: FC<RadioProps> = (props) => {
  const { children, ...otherProps } = props;

  return (
    <RadioAntd {...otherProps} className={s.radio}>
      {children}
    </RadioAntd>
  );
};

export default Radio;
