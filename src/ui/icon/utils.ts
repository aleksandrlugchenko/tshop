import { FC, SVGProps } from 'react';

import { CommonIcons } from './common/commonIcons';

export const Icons: Record<string, FC<SVGProps<SVGSVGElement>>> = {
  ...CommonIcons,
};
