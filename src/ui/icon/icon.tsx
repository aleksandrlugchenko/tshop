import { FC, memo } from 'react';

import { TIconsLiteral } from './types';
import { Icons } from './utils';

export interface IconProps {
  name: TIconsLiteral;
  size?: number;
  width?: number;
  height?: number;
  className?: string;
  color?: string;
  secondName?: TIconsLiteral;
  secondaryMainIconColor?: string;
}
const Icon: FC<IconProps> = (props) => {
  const { name, size = 24, width, height, className, color = 'currentColor' } = props;

  if (!name || !Icons[name]) return null;

  const MainInnerIcon = Icons[name];

  const mainIconProps = {
    width: width ?? size,
    height: height ?? size,
    className,
    color,
  };

  return <MainInnerIcon {...mainIconProps} />;
};

export default memo(Icon);
