import { Icons } from './utils';

export interface SVGIcon {
  width?: number;
  height?: number;
  size?: number;
  color?: string;
  className?: string;
}

export type TIconsLiteral = keyof typeof Icons;
