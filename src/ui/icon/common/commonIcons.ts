import { FC, SVGProps } from 'react';

import { ReactComponent as ChangeTheme } from './change-theme.svg';
import { ReactComponent as Filters } from './filters.svg';
import { ReactComponent as Logo } from './logo.svg';
import { ReactComponent as Search } from './search.svg';
import { ReactComponent as SimpleArrowRight } from './simpleArrowRight.svg';
import { ReactComponent as Sorting } from './sorting.svg';
import { ReactComponent as Star } from './star.svg';

export const CommonIcons: Record<string, FC<SVGProps<SVGSVGElement>>> = {
  logo: Logo,
  search: Search,
  changeTheme: ChangeTheme,
  simpleArrowRight: SimpleArrowRight,
  star: Star,
  sorting: Sorting,
  filters: Filters,
};
