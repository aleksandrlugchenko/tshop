import Icon, { IconProps } from './icon';
import { SVGIcon, TIconsLiteral } from './types';

export type { IconProps, SVGIcon, TIconsLiteral };
export default Icon;
