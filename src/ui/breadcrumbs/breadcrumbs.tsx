import React, { FC } from 'react';

import cn from 'classnames';
import { Link } from 'react-router-dom';

import Icon from '../icon';
import s from './Breadcrumbs.module.scss';

interface ItemRenderRoute {
  path?: string;
  name: string;
}

export interface BreadcrumbsProps {
  isLoading?: boolean;
  divider?: string | React.ReactElement;
  routes: ItemRenderRoute[];
  loader?: React.ReactElement;
  className?: string;
}

const Breadcrumbs: FC<BreadcrumbsProps> = (props) => {
  const {
    loader,
    isLoading,
    divider = <Icon name="simpleArrowRight" className={s.divider} width={8} height={8} />,
    routes = [],
    className = '',
  } = props;

  const breadcrumbRenderer = (breadcrumb: ItemRenderRoute, index: number) => {
    const key = `breadcrumb-${breadcrumb.name}-${index}`;
    const dividerElem = index !== 0 && divider;
    return !breadcrumb.path ? (
      <div className={cn(s.currentLocation, s.breadcrumbWrapper)} key={key}>
        {dividerElem}
        <div>{breadcrumb.name}</div>
      </div>
    ) : (
      <div className={s.breadcrumbWrapper} key={key}>
        {dividerElem}
        <Link to={breadcrumb.path}>{breadcrumb.name}</Link>
      </div>
    );
  };

  return (
    <div className={cn(s.wrapper, { [className]: className })}>
      {routes.map(breadcrumbRenderer)}
    </div>
  );
};

export default Breadcrumbs;
