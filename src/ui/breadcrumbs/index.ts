import Breadcrumbs, { BreadcrumbsProps } from './breadcrumbs';

export type { BreadcrumbsProps };
export default Breadcrumbs;
