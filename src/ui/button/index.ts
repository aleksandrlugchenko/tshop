import Button, { ButtonProps, ButtonSizeType } from './button';

export type { ButtonProps, ButtonSizeType };
export default Button;
