import InternalCheckbox, { CheckboxProps } from './checkbox';

export type CompoundedComponent = typeof InternalCheckbox;

const Checkbox = InternalCheckbox as CompoundedComponent;

export type { CheckboxProps };
export default Checkbox;
