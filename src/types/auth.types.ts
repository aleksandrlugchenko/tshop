export type LoginFormDataType = {
  email: string;
  password: string;
};

export type RegistrationFormDataType = LoginFormDataType & {
  email: string;
  password: string;
};
