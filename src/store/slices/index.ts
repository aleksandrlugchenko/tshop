import { appSlice } from './appSlice';
import { authSlice } from './userSlice';

export const slices = {
  [authSlice.name]: authSlice.reducer,
  [appSlice.name]: appSlice.reducer,
};
