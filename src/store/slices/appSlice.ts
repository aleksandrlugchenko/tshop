import { createSlice } from '@reduxjs/toolkit';

export const SLICE_NAME = 'appSlice';

export type AppState = {
  isSiderOpened: boolean | null;
};

const initialState: AppState = {
  isSiderOpened: null,
};

export const appSlice = createSlice({
  name: SLICE_NAME,
  initialState,
  reducers: {
    setIsSiderOpened(state, action) {
      state.isSiderOpened = action.payload;
    },
  },
});

export const { setIsSiderOpened } = appSlice.actions;
export default appSlice.reducer;
