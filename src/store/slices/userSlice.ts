import { createSlice } from '@reduxjs/toolkit';

import { loginEndpoint } from '../services/user-management/userManagementApiService';

export const SLICE_NAME = 'authSlice';

export type AuthState = {
  isLoggedIn: boolean;
};

const initialState: AuthState = {
  isLoggedIn: false,
};

export const authSlice = createSlice({
  name: SLICE_NAME,
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addMatcher(loginEndpoint.matchFulfilled, (state, action) => {
      state.isLoggedIn = true;
      window.localStorage.setItem('token', action.payload.token);
    });
  },
});

export default authSlice.reducer;
