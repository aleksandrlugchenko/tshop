import { RootState } from 'store';
import { AppState, SLICE_NAME } from 'store/slices/appSlice';

export const appStateSelector = (state: RootState): AppState => state[SLICE_NAME];

export const isSiderOpenedSelector = (state: RootState): boolean =>
  appStateSelector(state).isSiderOpened;
