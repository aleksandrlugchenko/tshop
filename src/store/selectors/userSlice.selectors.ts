import { AuthState, SLICE_NAME } from 'store/slices/userSlice';
import { RootState } from 'store';

export const userStateSelector = (state: RootState): AuthState => state[SLICE_NAME];

export const isLoggedInSelector = (state: RootState): boolean =>
  userStateSelector(state).isLoggedIn;
