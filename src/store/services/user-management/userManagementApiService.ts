import initialApiService from '../initialApiService';
import {
  LoginRequestPropsType,
  LoginResponseType,
  RegisterRequestPropsType,
  RegistrationResponseType,
} from './userManagement.api.types';

const userManagementApiService = initialApiService.injectEndpoints({
  endpoints: (builder) => ({
    login: builder.mutation<LoginResponseType, LoginRequestPropsType>({
      query: (data) => ({
        url: '/auth/login',
        method: 'POST',
        body: data,
      }),
    }),
    register: builder.mutation<RegistrationResponseType, RegisterRequestPropsType>({
      query: (data) => ({
        url: '/users',
        method: 'POST',
        body: data,
      }),
    }),
  }),
});

export const loginEndpoint = userManagementApiService.endpoints.login;

export const { useRegisterMutation, useLoginMutation } = userManagementApiService;
