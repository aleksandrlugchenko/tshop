import { LoginFormDataType, RegistrationFormDataType } from 'types/auth.types';
import { UserType } from 'types/user.types';

export type LoginRequestPropsType = Omit<LoginFormDataType, 'email'> & {
  username: string;
};
export type RegisterRequestPropsType = RegistrationFormDataType & {
  username: string;
};
export type RegistrationResponseType = UserType;
export type LoginResponseType = {
  token: string;
};
