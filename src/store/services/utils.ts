import { fetchBaseQuery } from '@reduxjs/toolkit/query';

export const BASE_QUERY_WITH_AUTH = fetchBaseQuery({
  baseUrl: 'https://fakestoreapi.com',
  credentials: 'same-origin',
  mode: 'cors',
});
