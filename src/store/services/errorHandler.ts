import { isRejectedWithValue, MiddlewareAPI } from '@reduxjs/toolkit';
import type { Middleware } from '@reduxjs/toolkit';
import message from 'ui/message';

export const rtkQueryErrorLogger: Middleware = (api: MiddlewareAPI) => (next) => (action) => {
  // RTK Query uses `createAsyncThunk` from redux-toolkit under the hood, so we're able to utilize these matchers!
  if (isRejectedWithValue(action)) {
    console.warn('We got a rejected action!');
    message.error('Something went wrong!');
  }

  return next(action);
};
