import { ProductType } from 'types/Product.types';

export type ProductsByCategoryRequestType = { category: string; limit?: number };

export type ProductsByCategoryResponseType = ProductType[];

export type SingleProductResponseType = ProductType;
