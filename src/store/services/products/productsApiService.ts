import initialApiService from '../initialApiService';
import {
  ProductsByCategoryRequestType,
  ProductsByCategoryResponseType,
  SingleProductResponseType,
} from './products.api.types';

const productsApiService = initialApiService.injectEndpoints({
  endpoints: (builder) => ({
    getProducts: builder.query({
      query: () => '/products',
    }),
    getCategories: builder.query<string[], void>({
      query: () => '/products/categories',
    }),
    getProductsByCategory: builder.query<
      ProductsByCategoryResponseType,
      ProductsByCategoryRequestType
    >({
      query: (data: ProductsByCategoryRequestType) =>
        `/products/category/${data.category}${data.limit ? `?limit=${data.limit}` : ''}`,
    }),
    getSingleProduct: builder.query<SingleProductResponseType, number>({
      query: (id: number) => `/products/${id}`,
    }),
  }),
});

export const {
  useLazyGetCategoriesQuery,
  useLazyGetProductsByCategoryQuery,
  useGetProductsByCategoryQuery,
  useGetCategoriesQuery,
  useLazyGetSingleProductQuery,
} = productsApiService;
