import { configureStore } from '@reduxjs/toolkit';
import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';

import initialApiService from './services/initialApiService';
import { rtkQueryErrorLogger } from './services/errorHandler';

import { slices } from './slices';

export const store = configureStore({
  reducer: {
    [initialApiService.reducerPath]: initialApiService.reducer,
    ...slices,
  },
  middleware: (getDefaultMiddleware) => {
    return getDefaultMiddleware({ serializableCheck: false }).concat(
      initialApiService.middleware,
      rtkQueryErrorLogger,
    );
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

// use these functions instead of useDispatch and useSelector
export const useAppDispatch: () => AppDispatch = useDispatch;
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
