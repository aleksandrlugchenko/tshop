export const formRules = {
  required: {
    required: true,
    message: 'The field cannot be empty',
  },
  email: {
    pattern: /\S+@\S+\.\S+/,
    message: 'Incorrect email address',
    validateTrigger: ['onBlur'],
  },
  password: {
    required: true,
    pattern: /^(?=.*\d)(?=.*[A-Z]).{6,}$/,
    message:
      'Your input must be at least 6 characters long and contain at least one digit and one uppercase letter',
  },
};
