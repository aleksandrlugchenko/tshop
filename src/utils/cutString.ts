export const cutString = (input: string, maxLength: number = 20): string => {
  if (input.length > maxLength) {
    return input.slice(0, maxLength) + '...';
  }
  return input;
};
