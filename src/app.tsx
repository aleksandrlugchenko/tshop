import { useEffect, useState } from 'react';

import { AppNavigation, AppPagesHeader, AuthPagesHeader } from 'components';
import Pages from 'pages';
import { Layout } from 'ui';

import { useAppSelector } from 'store';
import { isSiderOpenedSelector } from 'store/selectors/appSlice.selectors';
import { isLoggedInSelector } from 'store/selectors/userSlice.selectors';

import { ThemeType } from 'types/app.types';

import s from './App.module.scss';

const App = () => {
  const [theme, setTheme] = useState<ThemeType>(
    (localStorage.getItem('theme') as 'light' | 'dark') || 'light',
  );
  const isTokenValid = localStorage.getItem('token');
  const isLogged = useAppSelector(isLoggedInSelector);
  const isOpenedMenu = useAppSelector(isSiderOpenedSelector);
  const isAuth = isLogged || !!isTokenValid;

  useEffect(() => {
    document.body.className = theme;
    localStorage.setItem('theme', theme);
  }, [theme]);

  useEffect(() => {
    if (isOpenedMenu) {
      document.body.style.overflow = 'hidden';
    } else {
      document.body.style.overflow = 'unset';
    }
  }, [isOpenedMenu]);

  const toggleTheme = () => {
    setTheme((current) => (current === 'light' ? 'dark' : 'light'));
  };

  return (
    <div>
      <Layout isAuth={isAuth}>
        <Layout.Header>
          {!isAuth ? <AuthPagesHeader /> : <AppPagesHeader changeTheme={toggleTheme} />}
        </Layout.Header>
        <Layout.Content className={s.content} isAuth={isAuth}>
          <Pages />
        </Layout.Content>
        {isAuth && (
          <Layout.Sider opened={isOpenedMenu} className={s.sider}>
            <AppNavigation />
          </Layout.Sider>
        )}
      </Layout>
    </div>
  );
};

export default App;
